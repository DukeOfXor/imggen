#ifndef LINKED_LIST_H
#define LINKED_LIST_H

struct pixel {
	struct pixel *next_pixel_x;
	struct pixel *next_pixel_y;
	unsigned char color[3];
};

void generate_random_img(struct pixel *first_pixel, int width, int height);

int write_img(struct pixel *first_pixel, char *file_name);

#endif
