# README #

This is just a little application I wrote while learning C.
It can generate a random image which is saved in memory as a 2d linked list.

### TODO ###

The method `write_img()` in `linked_list_img.c` could be implemented to save the image on disk in some well known format (PNG, JPEG, BMP, ...).

This would require deeper understanding about the format. Because of this it has not been done yet, however if you have time and motivation to do this - feel free to create a pull request.