INCLUDEDIR=include
SRCDIR=src
OUTDIR=out
CC=gcc
CFLAGS=-Wall -I$(INCLUDEDIR)

SRCS=$(shell ls $(SRCDIR)/*.c 2> /dev/null)

compile: $(SRCS)
	$(CC) -o $(OUTDIR)/a $^ $(CFLAGS)
	
clean:
	rm -f $(OUTDIR)/*
	
init:
	mkdir $(INCLUDEDIR)
	mkdir $(SRCDIR)
	mkdir $(OUTDIR)