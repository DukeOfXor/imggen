#include <stdio.h>
#include <stdlib.h>
#include "linked_list_img.h"

void generate_random_img(struct pixel *first_pixel, int width, int height)
{
	printf("Generating random image with resolution %dx%d\n", width, height);
	fflush(stdin);
	
	struct pixel *prev_pixel = first_pixel;
	
	// create first row of image
	for (int i = 0; i < width - 1; i++) {
		// create new pixel
		struct pixel *next_pixel = (struct pixel*)malloc(sizeof(struct pixel));
		next_pixel->next_pixel_x = 0;
		next_pixel->next_pixel_y = 0;
		prev_pixel->color[0] = 0;
		prev_pixel->color[1] = 0;
		prev_pixel->color[2] = 0;
		// add pixel to chain
		prev_pixel->next_pixel_x = next_pixel;
		prev_pixel = next_pixel;
	}
	
	// create columns for each pixel in the first row
	struct pixel *pixel_x = first_pixel;
	do {
		prev_pixel = pixel_x;
		prev_pixel->color[0] = rand() % 256;
		prev_pixel->color[1] = rand() % 256;
		prev_pixel->color[2] = rand() % 256;
		
		for (int i = 0; i < height - 1; i++) {
			// create new pixel
			struct pixel *next_pixel = (struct pixel*)malloc(sizeof(struct pixel));
			next_pixel->next_pixel_x = 0;
			next_pixel->next_pixel_y = 0;
			next_pixel->color[0] = rand() % 256;
			next_pixel->color[1] = rand() % 256;
			next_pixel->color[2] = rand() % 256;
			// add pixel to chain
			prev_pixel->next_pixel_y = next_pixel;
			prev_pixel = next_pixel;
		}
	} while ((pixel_x = pixel_x->next_pixel_x) != 0);
}

int write_img(struct pixel *first_pixel, char *file_name)
{
  printf("Writing image to \"%s\"\n", file_name);
  fflush(stdin);
  return 0;
}
