#include <stdio.h>
#include <stdlib.h>

#include "linked_list_img.h"

int main()
{
	struct pixel *first_pixel = &(struct pixel) {};
	generate_random_img(first_pixel, 8000, 8000);

	int err = write_img(first_pixel, "img.png");

	if (err != 0) {
		printf("An error occured while writing the image");
		fflush(stdin);
		return 1;
	}

	return 0;
}
